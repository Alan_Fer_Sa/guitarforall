import os
import pathlib

import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
import tensorflow as tf
import random

from tensorflow.keras.layers.experimental import preprocessing
from tensorflow.keras import layers
from tensorflow.keras import models
from IPython import display

#For searh and list the files
import os


# import only system from os 
from os import system, name 
  
# import sleep to show output for some time period 
from time import sleep 
  
# define our clear function 
def clear(): 
  
    # for windows 
    if name == 'nt': 
        _ = system('cls') 
  
    # for mac and linux(here, os.name is 'posix') 
    else: 
        _ = system('clear') 

# Set seed for experiment reproducibility
seed = random.randint(0,42)
tf.random.set_seed(seed)
np.random.seed(seed)

# Data to trainn
Original = os.listdir('GuitarNotes/')
print(Original)
commands =[]
for i in Original:
    RemovedWav=i.replace('.wav','')
    commands.append(RemovedWav)
print(commands)
#Data argumented
Argumented = os.listdir('GuitarNotesArgumented/')
print(len(Argumented))
#Create a dictonary
#Join every data in a dictionary
DataDictionary={}
for i in Original:
    RemovedWav=i.replace('.wav','')
    Datos=[]
    for j in Argumented:
        if RemovedWav in j:
            Datos.append(j)
    DataDictionary[i]=Datos
print(len(DataDictionary))

#Create the data sets

CarpetOfData='GuitarNotesArgumented/'
#Split in traing 80% 10% 10%
Data2Traing=[]
Data2Test=[]
Data2Cross=[]

for i in DataDictionary.keys():
    ActualSet=DataDictionary[i]
    #Datos de ese set
    CountOfSet=len(ActualSet)
    #print(CountOfSet)
    TotalPerTestAndCross=int(CountOfSet*0.1)
    #print(TotalPerTestAndCross)
    TestIndex=[]
    CrossIndex=[]
    #Index for test validation
    for k in range(0,TotalPerTestAndCross):
        PositionTest=random.randint(0, CountOfSet)
        while(PositionTest in Data2Test):
            PositionTest=random.randint(0, CountOfSet)
        TestIndex.append(PositionTest)
    #Index for cross validation
    for k in range(0,TotalPerTestAndCross):
        PositionCross=random.randint(0, CountOfSet)
        while(PositionCross in Data2Test or PositionCross in CrossIndex):
            PositionCross=random.randint(0, CountOfSet)
        CrossIndex.append(PositionCross)
    #Split the data sets
    for j in range(0,CountOfSet):
        Nombre=CarpetOfData+ActualSet[j]
        if(j in TestIndex):
            Data2Test.append(Nombre)
        elif(j in CrossIndex):
            Data2Cross.append(Nombre)
        else :
            Data2Traing.append(Nombre)
            
#Part to suffle the data
random.shuffle(Data2Traing)
random.shuffle(Data2Cross)
random.shuffle(Data2Test)
print("Total to train:"+str(len(Data2Traing)))
print("Total to cross:"+str(len(Data2Cross)))
print("Total to test:"+str(len(Data2Test)))
Items2Train=len(Data2Traing)
Items2Cross=len(Data2Cross)
Items2Test=len(Data2Cross)

print("Preparing the functions to decode the audio")
#Special function to create a tensor from the audio
def decode_audio(audio_binary):
  audio, _ = tf.audio.decode_wav(audio_binary)
  return tf.squeeze(audio, axis=-1)

def get_label(file_path):
    WithOutInitial =tf.strings.regex_replace(file_path,CarpetOfData,'')
    Nombres=tf.strings.split(WithOutInitial,'_')
    return Nombres[0]

def get_waveform_and_label(file_path):
    label = get_label(file_path)
    print(file_path)
    audio_binary = tf.io.read_file(file_path)
    waveform = decode_audio(audio_binary)
    return waveform, label

print("Preparing the dataset")
AUTOTUNE = tf.data.experimental.AUTOTUNE
files_ds = tf.data.Dataset.from_tensor_slices(Data2Traing)
waveform_ds = files_ds.map(get_waveform_and_label, num_parallel_calls=AUTOTUNE)

print("Preparing the function to obtain the spectogram")
SizeOfMuestra=200100
SamplingFz=44100

def get_spectrogram(waveform):
  # Padding for files with less than 16000 samples
  print(tf.shape(waveform))
  zero_padding = tf.zeros([SizeOfMuestra] - tf.shape(waveform), dtype=tf.float32)

  # Concatenate audio with padding so that all audio clips will be of the 
  # same length
  waveform = tf.cast(waveform, tf.float32)
  equal_length = tf.concat([waveform, zero_padding], 0)
  spectrogram = tf.signal.stft(
      equal_length, frame_length=512, frame_step=720)

  spectrogram = tf.abs(spectrogram)

  return spectrogram
#Preparing the data

print("Preparing the function to convert the wave in spectogram and label")
SizeOfMuestra=200100
SamplingFz=44100

def get_spectrogram(waveform):
  # Padding for files with less than 16000 samples
  print(tf.shape(waveform))
  zero_padding = tf.zeros([SizeOfMuestra] - tf.shape(waveform), dtype=tf.float32)

  # Concatenate audio with padding so that all audio clips will be of the 
  # same length
  waveform = tf.cast(waveform, tf.float32)
  equal_length = tf.concat([waveform, zero_padding], 0)
  spectrogram = tf.signal.stft(
      equal_length, frame_length=512, frame_step=720)

  spectrogram = tf.abs(spectrogram)

  return spectrogram

def get_spectrogram_and_label_id(audio, label):
  spectrogram = get_spectrogram(audio)
  spectrogram = tf.expand_dims(spectrogram, -1)
  label_id = tf.argmax(label == commands)
  return spectrogram, label_id

print("Mapping")
spectrogram_ds = waveform_ds.map(
    get_spectrogram_and_label_id, num_parallel_calls=AUTOTUNE)

print("Preprocessing... the data sets")
def preprocess_dataset(files):
  files_ds = tf.data.Dataset.from_tensor_slices(files)
  output_ds = files_ds.map(get_waveform_and_label, num_parallel_calls=AUTOTUNE)
  output_ds = output_ds.map(
      get_spectrogram_and_label_id,  num_parallel_calls=AUTOTUNE)
  return output_ds

train_ds = spectrogram_ds
val_ds = preprocess_dataset(Data2Cross)
test_ds = preprocess_dataset(Data2Test)

#You will be increasing the batch size from 1/10 to 1/3 
#This arquitecture has an 0.75% of precision, but it works fine for us
batch_size = 64
train_ds = train_ds.batch(int(batch_size/10))
val_ds = val_ds.batch(int(Items2Cross/9))

train_ds = train_ds.cache().prefetch(AUTOTUNE)
val_ds = val_ds.cache().prefetch(AUTOTUNE)

#Neural model
print("Adjusting and creating the model")
for spectrogram, _ in spectrogram_ds.take(1):
  input_shape = spectrogram.shape

print('Input shape:', input_shape)
num_labels = len(commands)
print(num_labels)
norm_layer = preprocessing.Normalization()
norm_layer.adapt(spectrogram_ds.map(lambda x, _: x))

# if you change this arquitecture you will need to erase the content of the ModelsWeghts
model = models.Sequential([
    layers.Input(shape=input_shape),
    preprocessing.Resizing(16, 16), 
    norm_layer,
    layers.Conv2D(32, 3, activation='relu'),
    layers.Conv2D(64, 3, activation='relu'),
    layers.MaxPooling2D(),
    layers.Flatten(),
    layers.Dense(128, activation='relu'),
    layers.Dense(num_labels),
])

model.summary()

model.compile(
    optimizer=tf.keras.optimizers.Adam(),
    loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
    metrics=['accuracy'],
)

print("On training... please wait")

#Select the most older value
SavedWeights = os.listdir('ModelsWeight/')
#print(SavedWeights)
MoreHight=-1
for i in SavedWeights:
    if('index' in i):
        Value2Load=i.split('_')
        #print(Value2Load)
        Value2Load=Value2Load[1]
        #print(Value2Load)
        Value2Load=Value2Load.split('.')
        #print(Value2Load)
        Value2Load=Value2Load[0]
        #print(Value2Load)
        ValueIter=int(Value2Load)
        if(MoreHight<ValueIter):
            MoreHight=ValueIter

ValorInicial=0
if(MoreHight>0):
    ArchivoACargar='ModelsWeight/CPK_'+str(MoreHight)+'.ckpt'
    print('Pesos cargados: ')
    print(ArchivoACargar)
    model.load_weights(ArchivoACargar)
    ValorInicial=MoreHight+1

Iteracion=ValorInicial
while(True):
    print("Iniciando una nueva iteracion: "+str(Iteracion))
    EPOCHS = 50
    history = model.fit(
        train_ds, 
        validation_data=val_ds,  
        epochs=EPOCHS
    )
    metrics = history.history
    model.save_weights('ModelsWeight/CPK_'+str(Iteracion)+'.ckpt')
    #Check the accurancy by the error metric you can change that value
    if(metrics['loss'][-1]<=0.6):
        print('Reached accurancy')
        break
    sleep(1) 
    clear() 
    Iteracion=Iteracion+1


#plt.plot(history.epoch, metrics['loss'], metrics['val_loss']) This part is optional
plt.plot(history.epoch, metrics['loss'])
plt.legend(['loss', 'val_loss'])
plt.grid()
plt.show()

test_audio = []
test_labels = []

for audio, label in test_ds:
  test_audio.append(audio.numpy())
  test_labels.append(label.numpy())

test_audio = np.array(test_audio)
test_labels = np.array(test_labels)


y_pred = np.argmax(model.predict(test_audio), axis=1)
y_true = test_labels

test_acc = sum(y_pred == y_true) / len(y_true)
print(f'Test set accuracy: {test_acc:.0%}')

#The next part is on the Test Neural Net to do our tests
